
export function svg() {
	document.querySelectorAll("img.svg").forEach(function (img) {
	  var imgID = img.getAttribute("id");
	  var imgClass = img.getAttribute("class");
	  var imgURL = img.getAttribute("src");
	  var xhr = new XMLHttpRequest();
	  xhr.open("GET", imgURL, true);
	  xhr.onreadystatechange = function () {
		if (this.readyState === 4 && this.status === 200) {
		  try {
			var data = xhr.responseXML;
			if (data && data.querySelector("svg")) {
			  var svg = data.querySelector("svg");
			  if (!svg) {
				return; // If SVG is not found, exit the function
			  }
			  if (typeof imgID !== "undefined") {
				svg.setAttribute("id", imgID);
			  }
			  if (typeof imgClass !== "undefined") {
				svg.setAttribute("class", imgClass + " replaced-svg");
			  }
			  svg.removeAttribute("xmlns:a");
			  if (
				!svg.getAttribute("viewBox") &&
				svg.getAttribute("height") &&
				svg.getAttribute("width")
			  ) {
				svg.setAttribute(
				  "viewBox",
				  "0 0 " +
				  svg.getAttribute("width") +
				  " " +
				  svg.getAttribute("height")
				);
			  }
			  if (img.parentNode) {
				img.parentNode.replaceChild(svg, img);
			  }
			}
		  } catch (error) {
			console.error("Error processing SVG:", error);
		  }
		}
	  };
	  xhr.setRequestHeader(
		"Content-Type",
		"application/x-www-form-urlencoded; charset=UTF-8"
	  );
	  xhr.send();
	});
}
  