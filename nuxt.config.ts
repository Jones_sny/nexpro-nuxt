// https://nuxt.com/docs/api/configuration/nuxt-config
import dotenv from 'dotenv'

dotenv.config()

export default defineNuxtConfig({
  runtimeConfig: {
    public: {
      apiBaseUrl: process.env.API_BASE_URL,
      assetBaseUrl: process.env.ASSET_BASE_URL || 'https://theaspendtree-cms.digitalnex.com/assets/',
    },
    port: process.env.PORT
  },
  devtools: { enabled: true },
  ssr: true,
  modules: [
    '@nuxtjs/i18n', 
    '@nuxt/image',
  ],
  css: [
    'bootstrap/dist/css/bootstrap.css',
    '@/assets/css/main.css',
  ],
  swiper: {
    modules: ['navigation', 'pagination', 'thumbs'],
  },
  server: {
    port: 3000, // Use the environment variable or default to 3000
    host: '0.0.0.0', // listen on all network interfaces
  },
  i18n: {
    locales: [
      { code: 'en', name: 'EN' },
      { code: 'th', name: 'TH' },
    ],
    strategy: 'prefix_except_default',
    defaultLocale: 'en',
    detectBrowserLanguage: {
      useCookie: true,
      cookieKey: 'locals',
      alwaysRedirect: true,
      // fallbackLocale: ['en', 'th'],
    },
  },
  components: [
    {
      path: '~/components',
      pathPrefix: false,
    },
  ],
  app: {
    head: {
      htmlAttrs: { lang: 'en' },
      link: [
        {
          rel: 'stylesheet',
          href: 'https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200',
        },
        { rel: 'icon', type: 'image/png', sizes: '32x32', href: '/favicon-32x32.png' },
        { rel: 'icon', type: 'image/png', sizes: '16x16', href: '/favicon-16x16.png' },
      ],
      script: [
        {
          src: 'https://cdn.jsdelivr.net/npm/lazyload@2.0.0-rc.2/lazyload.js',
          defer: true,
        },
      ],
    },
  },
})
