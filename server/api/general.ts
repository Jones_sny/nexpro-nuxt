// https://theaspendtree-cms.digitalnex.com/items/General_setting_translations?fields[]=*&fields[]=id&fields[]=languages_code.code&filter[_and][0][General_setting_id]=1&page=1&limit=-1
import axios from 'axios';

export default defineEventHandler(async (event) => {
  const config = useRuntimeConfig(event);
  const urlApi = config.public.apiBaseUrl;

//   items/Test_lang?fields=*.*,translations_page_title.Page_title.*,SEO.translations.*
  try {
    const res = await axios.get(urlApi + 'items/General_setting_translations?fields[]=*.*.*');
    return res.data;
  } catch (error) {
    console.error('Error fetching data:', error.response ? error.response.data : error.message);
    throw createError({
      statusCode: error.response ? error.response.status : 500,
      statusMessage: error.message,
    });
  }
});
