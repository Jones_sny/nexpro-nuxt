import axios from 'axios';
import { parse } from 'url';
export default defineEventHandler(async (event) => {
const { pathname, query } = parse(event.req.url || '', true);
  const config = useRuntimeConfig(event);
  const urlApi = config.public.apiBaseUrl;

//   items/Test_lang?fields=*.*,translations_page_title.Page_title.*,SEO.translations.*
  try {
    const res = await axios.get(`${urlApi}items/Home_translations?limit=1&offset=${query.lang ? query.lang :0}&fields=*,*.*.*.*`);
    return res.data;
  } catch (error) {
    console.error('Error fetching data:', error.response ? error.response.data : error.message);
    throw createError({
      statusCode: error.response ? error.response.status : 500,
      statusMessage: error.message,
    });
  }
});
